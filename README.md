# cbr2cbz

CBR to CBZ conversion tool for GNU/Linux


## Requirements

* A GNU/Linux distro (sorry, no Windows support)
* The non-free package ``unrar-nonfree`` (check [unrar](https://archlinux.org/packages/?name=unrar))
* The free package ``zip``. Install it with ``sudo pacman install zip`` for arch, ``sudo apt install rar`` .
* Python (checked with Python 2.7 and Python 3.3)

## Usage

To convert a single CBR file use this (the script can't handle filenames with spaces):

``./cbr2cbz.py -f <cbr_filename_without_spaces>``

To convert an entire directory with CBR files in it use:

``cbr2cbz.py -d <directory>``

The script will put CBZ files in the same directory. Please remember that this tool don't support filenames with spaces. In case you need to rename many files check the [detox](https://archlinux.org/packages/community/x86_64/detox/) package.

## Introduction

CBR files use RAR format, a non-free format. If you use a GPL comics reader in your tablet it's likely it will have problems when reading CBR files. Convert them to CBZ with this script.

This project is forked of [cbr2cbz](https://github.com/oogg06/cbr2cbz).

## Contributions and PRs
Feel free to add issues and PRs. The maintainer should answer and merge if approved. 

## TODO: 
- add custom directory output arg
- add remove cbr file after convertion
- add the pdf to cbz from [mathewskevin](https://github.com/mathewskevin/pdf-to-cbz/blob/master/pdf_to_cbz.py)
- Check [Dapler's cbr2cbz script](https://github.com/Dapbler/cbr2cbz), notably for the compression settings.
- Cherrypick the merge requests for improvements from both projects. 
- improve README.md for contributions, PRs, etc
- add a requirements.txt file